#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '0.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: infracoders_full

short_description: This is an example module for Infracoders Vienna

description:
    - "This module will just print some text"

options:
    message:
        description:
            - The message to be printed
        required: true
    new:
        description:
            - Control to demo if the result of this module is changed or not
        required: false

author:
    - Toni Schmidbauer
'''

EXAMPLES = '''
# Pass in a message
- name: Test with a message
  infracoders_full:
    message: hello world

# pass in a message and have changed true
- name: Test with a message and changed output
  infracoders_full:
    message: hello world
    new: true

# using 'fail me' as message will fail the task
- name: Test with a message and changed output
  infracoders:
    message: fail me
    new: true
'''

RETURN = '''
original_message:
    description: The original name param that was passed in
    type: str
message:
    description: The output message that the sample module generates
'''

from ansible.module_utils.basic import AnsibleModule

import getpass

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        message=dict(type='str', required=True),
        new=dict(type='bool', required=False, default=False)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        original_message='',
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        return result

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    result['original_message'] = module.params['message']
    result['message'] = "The module is running as {}".format(getpass.getuser())

    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target
    if module.params['new']:
        result['changed'] = True

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    if module.params['message'] == 'fail me':
        module.fail_json(msg='You requested this to fail', **result)

    # set failed to false so the action module can query the failed key
    result['failed'] = False

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
