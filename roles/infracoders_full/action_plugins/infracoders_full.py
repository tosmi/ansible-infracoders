import getpass

from ansible.utils.display import Display

from ansible.plugins.action import ActionBase


# this only works in the plugin, the module has stdout and stderr
# redirected. one way to pass messages to the plugin and display them is
# via the result json document
display = Display()


class ActionModule(ActionBase):

    # run() is an abstract method of ActionBase so we have to override
    # it. It get's automatically called when the action plugins runs.
    def run(self, tmp=None, task_vars=None):
        if task_vars is None:
            task_vars = dict()

        display.warning("This is a warning")

        # display.verbose() is a little bit flaky. if you pass a dict
        # (facts is a dict) directly it will somehow try to call split
        # on it, which does not work. so copy the facts dict into a
        # string
        #
        # caplevel 0 means -v, 1 means -vv and so on
        display.verbose("task_vars: {}".format(task_vars), caplevel=0)

        # call the run() method of the base class (ActionBase)
        # for example this sets up the tmp path for the module
        result = super(ActionModule, self).run(tmp, task_vars)

        # this calls the module with the same name. `infracoder_full`
        # in our case.
        # if module_name is None it is set to self._task.action
        mod_result = self._execute_module(module_name=None, task_vars=task_vars)

        # display.v() is a wrapper for display.verbose("", caplevel=0)
        # there is also .vv and .vvv available
        display.v("mod_result: {}".format(mod_result))
        if mod_result['failed'] is True:
            display.warning("Hm, i should handle this case")

        # you can also call into other modules, for example the setup
        # module which is going to collect facts. But you have to set
        # module_args to {}. If module_args is None it is set to
        # self._task.args in _execute_module which means that
        # arguments for this module (infracoders_full) are passed
        # along. This includes the `message` parameter, which setup
        # does not understand and the call fails...
        facts = self._execute_module(module_name='setup', module_args={})

        display.v("facts: {}".format(facts))

        uptime = facts.get('ansible_facts').get('ansible_uptime_seconds')

        # it is also possible to call the setup module with parameters set
        #
        setup_module_args = dict(
             filter="ansible_lo",
             gather_subset='!all,!any,network'
        )
        facts = self._execute_module(module_name='setup', module_args=setup_module_args)
        display.vv("filtered facts: {}".format(facts))

        msg = "The plugin is running as user {}".format(getpass.getuser())

        result = dict(plugin_msg=msg, uptime=uptime)

        # check if mod_result is defined in case we comment out calling
        # the module
        if 'mod_result' in locals():
            result.update(mod_result)

        # set verbose to true so that we can see the message
        result['_ansible_verbose_always'] = True
        return result
